package verify

type (
	// Client represents a Verify API consumer.
	Client struct {
		AccountSID string
		authToken  string
	}
)

// NewClient creates a new Client
func NewClient(accountSID, authToken string) *Client {
	return &Client{
		AccountSID: accountSID,
		authToken:  authToken,
	}
}
