package verify

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"
)

type (
	// Carrier contains information about a number's carrier.
	Carrier struct {
		ErrorCode         string `json:"error_code"`
		Name              string `json:"name"`
		MobileCountryCode string `json:"mobile_country_code"`
		MobileNetworkCode string `json:"mobile_network_code"`
		Type              string `json:"type"`
	}

	// Lookup contains info about the number being verified.
	Lookup struct {
		Carrier *Carrier `json:"carrier"`
	}

	// VerificationInput contains information to perform a verification.
	VerificationInput struct {
		To      string `json:"to"`
		Channel string `json:"channel"`
	}

	// VerificationOutput is received when a verification request has been processed.
	VerificationOutput struct {
		SID              string                   `json:"sid"`
		ServiceSID       string                   `json:"service_sid"`
		AccountSID       string                   `json:"account_sid"`
		To               string                   `json:"to"`
		Channel          string                   `json:"channel"`
		Status           string                   `json:"status"`
		Valid            bool                     `json:"valid"`
		Lookup           *Lookup                  `json:"lookup"`
		Amount           string                   `json:"amount"`
		Payee            string                   `json:"payee"`
		SendCodeAttempts []map[string]interface{} `json:"send_code_attempts"`
		DateCreated      time.Time                `json:"date_created"`
		DateUpdated      time.Time                `json:"date_updated"`
		URL              string                   `json:"url"`
	}
)

// NewVerification kicks off a new verification attempt.
func (c *Client) NewVerification(
	ctx context.Context,
	serviceSID string,
	r *VerificationInput,
) (*VerificationOutput, error) {
	data := url.Values{}
	data.Set("To", r.To)
	data.Set("Channel", r.Channel)

	req, err := http.NewRequestWithContext(
		ctx,
		http.MethodPost,
		fmt.Sprintf("https://verify.twilio.com/v2/Services/%s/Verifications", serviceSID),
		strings.NewReader(data.Encode()),
	)
	if err != nil {
		return nil, err
	}

	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.SetBasicAuth(c.AccountSID, c.authToken)

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	if res.StatusCode != http.StatusCreated {
		errorPayload := Error{}
		if err := json.Unmarshal(body, &errorPayload); err != nil {
			return nil, err
		}

		return nil, &errorPayload
	}

	out := VerificationOutput{}
	if err := json.Unmarshal(body, &out); err != nil {
		return nil, err
	}

	return &out, nil
}
