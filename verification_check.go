package verify

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"
)

type (
	// VerificationCheckInput contains information to perform a verification.
	VerificationCheckInput struct {
		To   string `json:"to"`
		Code string `json:"code"`
	}

	// VerificationCheckOutput is received when a verification request has been processed.
	VerificationCheckOutput struct {
		SID         string    `json:"sid"`
		ServiceSID  string    `json:"service_sid"`
		AccountSID  string    `json:"account_sid"`
		To          string    `json:"to"`
		Channel     string    `json:"channel"`
		Status      string    `json:"status"`
		Valid       bool      `json:"valid"`
		Amount      string    `json:"amount"`
		Payee       string    `json:"payee"`
		DateCreated time.Time `json:"date_created"`
		DateUpdated time.Time `json:"date_updated"`
	}
)

// NewVerificationCheck kicks off a new verification check attempt.
func (c *Client) NewVerificationCheck(
	ctx context.Context,
	serviceSID string,
	r *VerificationCheckInput,
) (*VerificationCheckOutput, error) {
	data := url.Values{}
	data.Set("To", r.To)
	data.Set("Code", r.Code)

	req, err := http.NewRequestWithContext(
		ctx,
		http.MethodPost,
		fmt.Sprintf("https://verify.twilio.com/v2/Services/%s/VerificationCheck", serviceSID),
		strings.NewReader(data.Encode()),
	)
	if err != nil {
		return nil, err
	}

	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.SetBasicAuth(c.AccountSID, c.authToken)

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	if res.StatusCode != http.StatusOK {
		errorPayload := Error{}
		if err := json.Unmarshal(body, &errorPayload); err != nil {
			return nil, err
		}

		return nil, &errorPayload
	}

	out := VerificationCheckOutput{}
	if err := json.Unmarshal(body, &out); err != nil {
		return nil, err
	}

	return &out, nil
}
