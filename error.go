package verify

import "fmt"

type (
	// Error contains information about an API call failure.
	Error struct {
		Code     int    `json:"code"`
		Detail   string `json:"detail"`
		Message  string `json:"message"`
		MoreInfo string `json:"more_info"`
		Status   int    `json:"status"`
	}
)

func (e *Error) Error() string {
	return fmt.Sprintf("verify: %s", e.Message)
}
