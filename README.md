# Verify

An unofficial Go SDK for Twilio's Verify service. It requires Go 1.13.

## Examples

The `examples` directory contains demonstrations of this package in action.