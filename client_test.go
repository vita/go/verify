package verify_test

import (
	"testing"

	"gitlab.com/vita/go/verify"
)

func TestNewClient(t *testing.T) {
	client := verify.NewClient("hello", "world")
	if client == nil {
		t.Error("expected `*verify.Client`, got nil")
	}
}
