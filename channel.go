package verify

const (
	// ChannelCall sends a verification challenge through a phone call.
	ChannelCall = "call"

	// ChannelEmail sends a verification challenge through email.
	ChannelEmail = "email"

	// ChannelSMS sends a verification challenge through an SMS message.
	ChannelSMS = "sms"
)
