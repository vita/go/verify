package verify

const (
	// StatusApproved indicates the verification attempt was successful.
	StatusApproved = "approved"

	// StatusDenied indicates the verification attempt was unsuccessful.
	StatusDenied = "denied"

	// StatusPending indicates the verification attempt has not been completed.
	StatusPending = "pending"
)
