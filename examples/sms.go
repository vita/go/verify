package main

import (
	"bufio"
	"context"
	"fmt"
	"log"
	"os"
	"strings"

	"gitlab.com/vita/go/verify"
)

func main() {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter number: ")
	number, _ := reader.ReadString('\n')

	client := verify.NewClient(
		os.Getenv("TWILIO_ACCOUNT_SID"),
		os.Getenv("TWILIO_ACCOUNT_SECRET"),
	)

	// Sends an SMS code
	_, err := client.NewVerification(
		context.TODO(),
		os.Getenv("TWILIO_SERVICE_SID"),
		&verify.VerificationInput{
			To:      number,
			Channel: verify.ChannelSMS,
		},
	)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Print("Enter code: ")
	code, _ := reader.ReadString('\n')
	code = strings.TrimSpace(code)
	out, err := client.NewVerificationCheck(
		context.TODO(),
		os.Getenv("TWILIO_SERVICE_SID"),
		&verify.VerificationCheckInput{
			To:   number,
			Code: code,
		},
	)
	if err != nil {
		log.Fatal(err)
	}

	switch out.Status {
	case verify.StatusApproved:
		fmt.Println("Approved!")
	case verify.StatusDenied:
		fmt.Println("Denied :(")
	case verify.StatusPending:
		fmt.Println("Still pending...")
	}
}
